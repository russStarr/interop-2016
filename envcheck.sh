#!/bin/bash
#
# This script will check the build environment to make sure required dependencies are in place.
#

exitCode=0

# simple check to make sure required executables are in the $PATH
for req in yamllint behave VBoxHeadless vagrant ansible; do
  EVAL=`which $req`
  EVALCODE=$?
  if [ $EVALCODE = 1 ]; then
    echo "ERROR: $req not found... please see requirements at $CI_BUILD_REPO"
    exitCode=1
  elif [ $EVALCODE == 0 ]; then
    echo "Successfully found $req at $EVAL"
  fi
done

# add version checks here later if needed

# exit and display results
if [ $exitCode = 0 ]; then
  echo ; echo "Environment check completed successfully." ; exit 0
elif [ $exitCode = 1 ]; then
  echo ; echo "Environment check failed. See messages above." ; exit 1
fi
